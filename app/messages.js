const express = require('express');
const router = express.Router();

const createRouter = (db) => {
    router.get('/', (req, res) => {
        db.getMessages().then(promises => {
            Promise.all(promises).then(messages => {
                const parsedMessages = messages.map(mess => JSON.parse(mess));
                res.send(parsedMessages);
            });
        });
    });

    router.post('/', (req, res) => {
        const message = req.body;

        db.addMessage(message).then(result => {
            res.send(result);
        });

    });

    return router;
};

module.exports = createRouter;