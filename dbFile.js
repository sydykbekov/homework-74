const fs = require('fs');

module.exports = {
    getMessages: () => {
        return new Promise((resolve, reject) => {
            const path = "./messages";

            fs.readdir(path, (err, files) => {
                const messagesArray = [];

                files.slice(-5).forEach(file => {
                    messagesArray.push(new Promise((resolve, reject) => {
                        fs.readFile((path + '/' + file), (err, res) => {
                            if (err) {
                                reject(err);
                            } else {
                                resolve(res);
                            }
                        });
                    }));
                });

                resolve(messagesArray);
            });
        });
    },
    addMessage: (message) => {
        message.dateTime = new Date().toISOString();
        const fileName = `./messages/${message.dateTime}.txt`;

        return new Promise((resolve, reject) => {
            fs.writeFile(fileName, JSON.stringify(message), (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(message);
                }
            });
        });
    }
};