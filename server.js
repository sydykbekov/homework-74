const express = require('express');
const app = express();
const messages = require('./app/messages');
const dbFile = require('./dbFile');

const port = 8000;

app.use(express.json());

app.use('/messages', messages(dbFile));

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});